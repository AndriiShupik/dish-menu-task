import React from "react";
import Layout from "./components/common/Layout";

import "bootstrap/dist/css/bootstrap.css";
import "./css/fonts.css";
import "./css/app.css";

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
