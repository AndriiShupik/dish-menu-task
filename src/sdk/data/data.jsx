import DishModel from "../models/DishModel";
import IngrediendModel from "../models/IngrediendModel";

const defaultIngredientList = [
  new IngrediendModel({
    name: "meat",
    weight: 243,
  }),
];

const fetchedDishesList = [
  new DishModel({
    name: "Roasted Butternut Pumpkin, Shiitake Mushroom and Haloumi Salad",
    description:
      "A hearty mix of fresh greens, roasted vegetables and golden haloumi makes up this tasty winter salad.",
    price: 66,
    category: "meat",
    image: "images/dishes/Dish 1.png",
    ingredientList: defaultIngredientList,
  }),
  new DishModel({
    name: "Slow-cooked, Italian Beef Cheek Ragú with Pappardelle",
    description:
      "Slow-cooked beef cheek ragù. Serve with just-cooked pappardelle and sprinkle with lashings of Parmesan, of course.",
    price: 28,
    category: "meat",
    image: "images/dishes/Dish 2.png",
    ingredientList: defaultIngredientList,
  }),
  new DishModel({
    name: "Chicken Cotoletta with Brussels Sprouts, Rocket and Hazelnut Salad",
    description: "The super-crispy outer also happens to be gluten free.",
    price: 23,
    category: "meat",
    image: "images/dishes/Dish 3.png",
    ingredientList: defaultIngredientList,
  }),
  new DishModel({
    name: "Roasted Butternut Pumpkin, Shiitake Mushroom and Haloumi Salad",
    description:
      "A hearty mix of fresh greens, roasted vegetables and golden haloumi makes up this tasty winter salad.",
    price: 66,
    category: "meat",
    image: "images/dishes/Dish 4.png",
    ingredientList: defaultIngredientList,
  }),
  new DishModel({
    name: "Slow-cooked, Italian Beef Cheek Ragú with Pappardelle",
    description:
      "Slow-cooked beef cheek ragù. Serve with just-cooked pappardelle and sprinkle with lashings of Parmesan, of course.",
    price: 28,
    category: "meat",
    image: "images/dishes/Dish 5.png",
    ingredientList: defaultIngredientList,
  }),
  new DishModel({
    name: "Chicken Cotoletta with Brussels Sprouts, Rocket and Hazelnut Salad",
    description: "The super-crispy outer also happens to be gluten free.",
    price: 23,
    category: "meat",
    image: "images/dishes/Dish 6.png",
    ingredientList: defaultIngredientList,
  }),
];

export default fetchedDishesList;
