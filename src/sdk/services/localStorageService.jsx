export class LocalStorageService {
  constructor(name) {
    this.Name = name;
  }

  set(data) {
    try {
      localStorage.setItem(this.Name, JSON.stringify(data));
      return true;
    } catch (e) {
      if (e.name === "QUOTA_EXCEEDED_ERR") {
        console.log("Cannot save. Memory is full.Maybe delete a few items?");
      } else {
        console.log("Cannot save. Something went wrong");
      }
      return false;
    }
  }

  get() {
    const data = JSON.parse(localStorage.getItem(this.Name));
    return data;
  }

  remove() {
    localStorage.removeItem(this.Name);
  }

  clearAll() {
    localStorage.clear();
  }
}
