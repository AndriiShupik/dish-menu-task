import fetchedDishesList from "../data/data";
import { LocalStorageService } from "./localStorageService";

const FETCH_SIZE = 10;

class DishesService {
  constructor() {
    this.storage = new LocalStorageService("DishesList");
    const dishesListFromStore = this.storage.get();
    //Add data to store if empty
    if (!dishesListFromStore || dishesListFromStore.length === 0) {
      this.storage.set(fetchedDishesList);
    }
  }

  addDish(dish) {
    const allDishes = this.storage.get();
    allDishes.push(dish);
    this.storage.set(allDishes);
  }

  editDish(dish) {
    const allDishes = this.storage.get();
    const index = allDishes.findIndex((i) => i.id === dish.id);
    allDishes[index] = dish;
    this.storage.set(allDishes);
  }

  getDishesList(searchQuery) {
    searchQuery = searchQuery.toLowerCase();
    let dishesList = [];
    const allDishes = this.storage.get();
    if (
      searchQuery &&
      typeof searchQuery === "string" &&
      searchQuery.length > 0
    ) {
      dishesList = allDishes.filter((dish) => {
        const lowerCaseDishName = dish.name.toLowerCase();
        const ingredient = dish.ingredientList.find((ingredient) => {
          const lowerCaseIngredientName = ingredient.name.toLowerCase();
          return lowerCaseIngredientName.indexOf(searchQuery) >= 0;
        });

        return lowerCaseDishName.indexOf(searchQuery) >= 0 || ingredient;
      });
    } else {
      dishesList = allDishes;
    }
    return dishesList;
  }

  fetchDishes(index, searchQuery) {
    const dishesByQuery = this.getDishesList(searchQuery);
    return dishesByQuery.slice(index, index + FETCH_SIZE);
  }
}

const dishesServiceInstance = new DishesService();

export default dishesServiceInstance;
