import { v4 as uuidv4 } from "uuid";

export const WEIGHT_UNIT = "Kcl";

export function getTotalWeight(ingredientList) {
  return ingredientList && ingredientList.length > 0
    ? ingredientList.reduce((accumulator, curIngredient) => {
        const weight = parseInt(curIngredient.weight);
        const valid = weight && !isNaN(weight) ? weight : 0;
        return accumulator + valid;
      }, 0)
    : 0;
}

export default class IngrediendModel {
  constructor({ name, weight }) {
    this.id = uuidv4();
    this.name = name ?? "";
    this.weight = weight ?? "";
  }
}
