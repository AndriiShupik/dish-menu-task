import { v4 as uuidv4 } from "uuid";

export default class DishModel {
  constructor({ name, category, description, price, image, ingredientList }) {
    this.id = uuidv4();
    this.name = name ?? "";
    this.category = category ?? "";
    this.description = description ?? "";
    this.price = price ?? 0;
    this.image = image ?? "";
    this.ingredientList = ingredientList ?? [];
  }
}
