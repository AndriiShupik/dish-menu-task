import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col, Button } from "react-bootstrap";
import { Plus } from "react-bootstrap-icons";

class DishesListHeader extends Component {
  render() {
    return (
      <>
        <Col lg={12}>
          <Row className="dishes-list-header">
            <Col lg={8} sm={8} xs={12}>
              <Row>
                <span className="dish-list-title">Meat Dishes</span>
              </Row>
              <Row>
                <span className="dish-list-subtitle">
                  Some of the best meat dishes from worldwide
                </span>
              </Row>
            </Col>
            <Col className="dish-add-button-wrap" lg={4} sm={4} xs={12}>
              <Button
                className="dish-add-button align-bottom"
                variant="warning"
                onClick={() => this.props.onOpenModal()}
              >
                <span>
                  Add a new dish <Plus size={30} />
                </span>
              </Button>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

DishesListHeader.propTypes = {
  onOpenModal: PropTypes.func,
};

DishesListHeader.defaultProps = {
  onOpenModal: () => {},
};

export default DishesListHeader;
