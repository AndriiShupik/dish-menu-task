import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Row, Col } from "react-bootstrap";
import { Plus } from "react-bootstrap-icons";
import DishIngredientListItem from "./DishIngredientListItem";
import { WEIGHT_UNIT } from "../../sdk/models/IngrediendModel";

class DishListModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = { validated: false };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    const isValid = form.checkValidity();

    if (isValid) this.props.onSubmit();

    this.setState({ validated: !isValid });
  };

  render() {
    const {
      onChange,
      item,
      onEditIngredient,
      onRemoveIngredient,
      onAddIngredient,
    } = this.props;

    const { validated } = this.state;

    return (
      <Form
        noValidate
        validated={validated}
        onSubmit={this.handleSubmit}
        id="dish-modal-form-validate"
        className="dish-modal-form"
      >
        <Row
          className={`justify-content-md-center dish-form ${
            item && item.ingredientList ? "total-show" : ""
          }`}
        >
          <Col xs lg="8">
            <Form.Group>
              <Form.Control
                required
                type="text"
                name="name"
                placeholder="Dish name"
                autoComplete="false"
                maxLength={50}
                onChange={onChange}
                value={item.name}
              />
            </Form.Group>
            <Form.Group>
              <Form.Control
                required
                as="select"
                name="category"
                onChange={onChange}
                value={item.category}
              >
                <option value="">Select a dish category</option>
                <option value="meat">Meat</option>
                <option value="vegetables">Vegetables</option>
                <option value="cakes">Cakes</option>
              </Form.Control>
            </Form.Group>
            <Form.Group>
              <Form.Control
                required
                as="textarea"
                placeholder="Dish description"
                maxLength={150}
                rows={4}
                name="description"
                onChange={onChange}
                value={item.description}
              />
            </Form.Group>
            <Row className="ingredient-list-header">
              <Col className="ingredient-list-title">Ingredients</Col>
              <Col className=" text-right">
                <span
                  className="ingredient-list-add "
                  onClick={() => onAddIngredient()}
                >
                  Add a new ingredient
                  <Plus size={28} />
                </span>
              </Col>
            </Row>
            <Row>
              <Col>
                {item &&
                  item.ingredientList &&
                  item.ingredientList.map((value, index) => (
                    <DishIngredientListItem
                      index={index}
                      item={value}
                      removeItem={onRemoveIngredient}
                      editItem={onEditIngredient}
                    />
                  ))}
              </Col>
            </Row>
          </Col>
        </Row>
        {item && item.ingredientList && item.ingredientList.length > 0 && (
          <Row className="justify-content-md-center">
            <Col xs lg="8">
              <Row className="text-header">
                <Col className="ingredient-count">
                  <b>{item.ingredientList.length} Ingredients</b> in your dish
                </Col>
                <Col className="ingredient-count text-right">
                  Total weight :{" "}
                  <b>{` ${item.totalWeight ?? 0} ${WEIGHT_UNIT}`}</b>
                </Col>
              </Row>{" "}
            </Col>
          </Row>
        )}
      </Form>
    );
  }
}

DishListModalForm.propTypes = {
  item: PropTypes.object,
  onChange: PropTypes.func,
  onEditIngredient: PropTypes.func,
  onRemoveIngredient: PropTypes.func,
  onAddIngredient: PropTypes.func,
  onSubmit: PropTypes.func,
};

DishListModalForm.defaultProps = {
  onChange: () => {},
  onEditIngredient: () => {},
  onRemoveIngredient: () => {},
  onAddIngredient: () => {},
  onSubmit: () => {},
};

export default DishListModalForm;
