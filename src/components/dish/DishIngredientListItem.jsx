import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col, Form } from "react-bootstrap";
import { Trash, Plus } from "react-bootstrap-icons";
import { WEIGHT_UNIT } from "../../sdk/models/IngrediendModel";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

class DishIngredientListItem extends Component {
  render() {
    const { item, removeItem, editItem, index } = this.props;
    const { name, weight } = item;

    return (
      <Row>
        <Col lg={8} sm={8} xs={7}>
          <Row>
            <Col lg={1} sm={1} xs={1}>
              <span className="remove-ingredient-button">
                <FontAwesomeIcon icon={faBars} size={17} />
              </span>
            </Col>
            <Col>
              <Form.Group>
                <Form.Control
                  required
                  type="text"
                  placeholder="Ingredient name"
                  value={name}
                  name="name"
                  onChange={(e) => editItem(e, index)}
                />
                <span className="ingredient-unit">
                  <Plus />
                </span>
              </Form.Group>
            </Col>
          </Row>
        </Col>
        <Col>
          <Form.Group>
            <Form.Control
              required
              type="number"
              placeholder={`Weight (${WEIGHT_UNIT})`}
              value={`${weight}`}
              name="weight"
              onChange={(e) => editItem(e, index)}
            />
            <span className="ingredient-unit ">{WEIGHT_UNIT}</span>
          </Form.Group>
        </Col>
        <Col lg={1} sm={1} xs={1}>
          <Row>
            <span
              className="remove-ingredient-button"
              onClick={() => removeItem(item.id)}
            >
              <Trash size={17} />
            </span>
          </Row>
        </Col>
      </Row>
    );
  }
}

DishIngredientListItem.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  editItem: PropTypes.func,
  removeItem: PropTypes.func,
};

DishIngredientListItem.defaultProps = {
  removeItem: () => {},
  editItem: () => {},
};

export default DishIngredientListItem;
