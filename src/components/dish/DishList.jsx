import React, { Component } from "react";
import PropTypes from "prop-types";
import { Row, Col, Spinner } from "react-bootstrap";
import DishListItem from "./DishListItem";
import InfiniteScroll from "react-infinite-scroller";

class DishList extends Component {
  render() {
    const { items, onLoadMore, hasMore, onOpenModal } = this.props;

    return (
      <InfiniteScroll
        dataLength={items.length}
        loadMore={onLoadMore}
        hasMore={hasMore}
        endMessage={
          <p style={{ textAlign: "center" }}>
            <b>Yay! You have seen it all</b>
          </p>
        }
        loader={
          <Row>
            <Col className="text-center">
              <Spinner animation="border" variant="warning" />
            </Col>
          </Row>
        }
        // getScrollParent={() => this.props.scrollRef}
      >
        <Row lg={2} sm={1} xs={1} xl={3}>
          {items.length > 0 &&
            items.map((dish, index) => (
              <Col key={index}>
                <DishListItem item={dish} onClick={onOpenModal} />
              </Col>
            ))}
        </Row>
      </InfiniteScroll>
    );
  }
}

DishList.propTypes = {
  items: PropTypes.array,
  hasMore: PropTypes.bool,
  onLoadMore: PropTypes.func,
  onOpenModal: PropTypes.func,
};

DishList.defaultProps = {
  items: [],
  hasMore: true,
  onLoadMore: () => {},
  onOpenModal: () => {},
};

export default DishList;
