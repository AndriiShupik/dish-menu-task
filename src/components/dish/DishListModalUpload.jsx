import PropTypes from "prop-types";
import React, { Component } from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
import { Camera } from "react-bootstrap-icons";

class DishListModalUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: "",
      imagePreviewUrl: "",
    };
    this.fileInput = React.createRef();
    this.handleImageChange = this.handleImageChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.item !== this.props.item) {
      this.setState({ imagePreviewUrl: this.props.item.image || "" });
    }
  }

  handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result,
      });
      this.props.onImageChange(reader.result);
    };
    reader.readAsDataURL(file);
  }

  triggerInputFile = () => {
    if (
      this.fileInput.current !== undefined &&
      this.fileInput.current.click !== undefined
    )
      this.fileInput.current.click();
  };

  render() {
    let { imagePreviewUrl } = this.state;
    const { item } = this.props;
    return (
      <>
        <Col
          lg={12}
          sm={12}
          xs={12}
          className={`dish-modal-header ${
            imagePreviewUrl ? "image-exist" : ""
          }`}
        >
          <Container>
            {imagePreviewUrl && <Image src={imagePreviewUrl} fluid />}
            <Row className="justify-content-md-center">
              <Col xs lg="8" className="diah-modal-header-caption">
                <Row>
                  <Col>
                    <span className="dish-modal-title">
                      {item.name ? item.name : "Add a new dish"}
                    </span>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <span className="dish-modal-subtitle">
                      {item.description
                        ? item.description
                        : "Please enter all informations about your new dish"}
                    </span>
                  </Col>
                </Row>
                <input
                  type="file"
                  onChange={this.handleImageChange}
                  ref={this.fileInput}
                  accept="image/png, image/jpeg"
                />
              </Col>
              <Col
                xs
                lg="8"
                className="diah-modal-header-add-image-button-wrap"
              >
                <span
                  variant="default"
                  onClick={() => this.triggerInputFile()}
                  className="add-image-button"
                >
                  {imagePreviewUrl ? "Change the photo" : "Add a photo"}{" "}
                  <Camera size={24} />
                </span>
              </Col>
            </Row>
          </Container>
        </Col>
      </>
    );
  }
}

DishListModalUpload.propTypes = {
  item: PropTypes.object,
  onImageChange: PropTypes.func.isRequired,
};

DishListModalUpload.defaultProps = {
  onImageChange: () => {},
};

export default DishListModalUpload;
