import PropTypes from "prop-types";
import React, { Component } from "react";
import { Container } from "react-bootstrap";
import DishModel from "../../sdk/models/DishModel";
import dishesServiceInstance from "../../sdk/services/dishesService";
import DishList from "./DishList";
import DishListHeader from "./DishListHeader";
import DishListModal from "./DishListModal";
import "./style.css";

class DishListContainer extends Component {
  constructor() {
    super();
    this.state = { isModalOpen: false };
    this.state = {
      dishes: dishesServiceInstance.fetchDishes(
        0,
        this.props ? this.props.searchInputValue : ""
      ),
      hasMore: true,
      isModalOpen: false,
      currentDish: null,
    };
    this.getDishes = this.getDishes.bind(this);
    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.searchInputValue !== this.props.searchInputValue) {
      this.getDishes(0);
    }
  }

  onOpenModal = (item) => {
    this.setState({
      isModalOpen: true,
      currentDish: item || new DishModel({}),
    });
  };

  onCloseModal = () => {
    this.setState({ isModalOpen: false, currentDish: null });
  };

  getDishes = (index) => {
    const { dishes } = this.state;
    const fetchDishes = dishesServiceInstance.fetchDishes(
      index,
      this.props.searchInputValue
    );

    if (fetchDishes.length === 0) {
      this.setState({ hasMore: false });
      return;
    }
    if (index === 0) {
      this.setState({
        dishes: [...fetchDishes],
      });
    } else {
      this.setState({
        dishes: [...dishes, ...fetchDishes],
      });
    }
  };

  handleSubmit = (item) => {
    const { dishes } = this.state;
    const index = dishes.findIndex((i) => i.id === item.id);
    if (index >= 0) {
      dishes[index] = item;
      dishesServiceInstance.editDish(item);
      this.setState({ dishes });
    } else {
      dishesServiceInstance.addDish(item);
      this.setState({
        dishes: [...dishes, item],
      });
    }
    this.onCloseModal();
  };

  render() {
    const { dishes, hasMore, isModalOpen, currentDish } = this.state;

    return (
      <Container className="dish-list-container">
        <DishListHeader onOpenModal={this.onOpenModal} />
        <DishList
          items={dishes}
          onLoadMore={() => this.getDishes(dishes.length)}
          hasMore={hasMore}
          onOpenModal={this.onOpenModal}
        />
        <DishListModal
          item={currentDish}
          isModalOpen={isModalOpen}
          handleSubmit={this.handleSubmit}
          handleClose={this.onCloseModal}
        />
      </Container>
    );
  }
}
DishListContainer.propTypes = {
  searchInputValue: PropTypes.string,
};

DishListContainer.defaultProps = {
  searchInputValue: "",
};

export default DishListContainer;
