import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal, Button } from "react-bootstrap";
import DishListModalForm from "./DishListModalForm";
import DishListModalUpload from "./DishListModalUpload";
import { Plus } from "react-bootstrap-icons";
import IngrediendModel, {
  getTotalWeight,
  WEIGHT_UNIT,
} from "../../sdk/models/IngrediendModel";

class DishListModal extends Component {
  constructor(props) {
    super(props);
    this.state = { item: {} };
    this.renderImageForm = this.renderImageForm.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onHandleSubmit = this.onHandleSubmit.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.item !== this.props.item) {
      this.setState({ item: this.props.item || {} });
    }
  }

  onInputChange = (e) => {
    const { name, value } = e.target;
    this.setState({ item: { ...this.state.item, [name]: value } });
  };

  onEditIngredient = (e, index) => {
    const { item } = this.state;
    const { ingredientList } = item;
    const { name, value } = e.target;

    let editIngredientList = [...ingredientList];
    editIngredientList[index] = {
      ...editIngredientList[index],
      [name]: value,
    };

    this.setState({
      item: {
        ...this.state.item,
        ingredientList: editIngredientList,
      },
    });
  };

  onRemoveIngredient = (id) => {
    const { item } = this.state;
    const { ingredientList } = item;
    const newIngredientList = ingredientList.filter((i) => i.id !== id);

    this.setState({
      item: { ...this.state.item, ingredientList: newIngredientList },
    });
  };

  onAddIngredient = () => {
    this.setState({
      item: {
        ...this.state.item,
        ingredientList: [
          ...this.state.item.ingredientList,
          new IngrediendModel({}),
        ],
      },
    });
  };

  renderImageForm = () => {
    const { item } = this.state;

    return (
      <DishListModalUpload
        item={item}
        onImageChange={(newImage) =>
          this.setState({ item: { ...item, image: newImage } })
        }
      />
    );
  };

  onHandleSubmit = () => {
    const { handleSubmit } = this.props;
    const { item } = this.state;

    handleSubmit(item);
  };

  render() {
    const { isModalOpen, handleClose } = this.props;
    const { item } = this.state;
    const computedItem = {
      ...item,
      totalWeight: getTotalWeight(item.ingredientList),
    };

    return (
      <>
        <Modal
          show={isModalOpen}
          onHide={handleClose}
          dialogClassName="dish-modal"
          size="lg"
        >
          <Modal.Header className="dish-modal-header-wrap">
            {this.renderImageForm()}
          </Modal.Header>
          <Modal.Body>
            <DishListModalForm
              onSubmit={this.onHandleSubmit}
              item={computedItem}
              onChange={this.onInputChange}
              onEditIngredient={this.onEditIngredient}
              onRemoveIngredient={this.onRemoveIngredient}
              onAddIngredient={this.onAddIngredient}
            />
          </Modal.Body>
          <Modal.Footer className="dish-modal-footer">
            <Button
              type="submit"
              variant="warning"
              form="dish-modal-form-validate"
              className="dish-add-button"
            >
              Add dish to my menu{" "}
              {item.ingredientList &&
                item.ingredientList.length > 0 &&
                `${computedItem.totalWeight} ${WEIGHT_UNIT}`}
              <Plus size={28} className="dish-modal-add-plus-icon" />
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

DishListModal.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
  handleSubmit: PropTypes.func,
  item: PropTypes.object,
};

DishListModal.defaultProps = {
  handleClose: () => {},
  handleSubmit: () => {},
};

export default DishListModal;
