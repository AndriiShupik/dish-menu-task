import React, { Component } from "react";
import { Card, Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";
import { WEIGHT_UNIT, getTotalWeight } from "../../sdk/models/IngrediendModel";
import noImage from "../../images/dishes/capImage.png";

class DishListItem extends Component {
  render() {
    const { item, onClick } = this.props;
    const { name, description, price, category, image, ingredientList } = item;

    return (
      <Card className="dish-list-item">
        <Card.Img onClick={() => onClick(item)} variant="top" src={image || noImage} />
        <Card.Body>
          <Card.Title>
            <Row className="text-header">
              <Col className="dish-category">{category}</Col>
              <Col className="dish-weight">{`${getTotalWeight(ingredientList)} ${WEIGHT_UNIT}`}</Col>
            </Row>
          </Card.Title>
          <Row>
            <Col className="dish-name">{name}</Col>
          </Row>
          <Row>
            <Col className="dish-description">{description}</Col>
          </Row>
          <Row className="footer-text">
            <Col className="dish-price">{`$${price}`}</Col>
            <Col className="text-right">For 2 persons</Col>
          </Row>
        </Card.Body>
      </Card>
    );
  }
}

DishListItem.propTypes = {
  item: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

DishListItem.defaultProps = {
  onClick: () => {},
};

export default DishListItem;
