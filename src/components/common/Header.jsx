import React, { Component } from "react";
import { Container, Form, InputGroup, Nav, Navbar } from "react-bootstrap";
import { Search, Person } from "react-bootstrap-icons";
import logo from "../../images/dish-logo.png";
import PropTypes from "prop-types";

class Header extends Component {
  state = { headerWrapClassName: "transparent" };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    if (window.pageYOffset < 190) {
      if (!this.state.headerWrapClassName) {
        this.setState({ headerWrapClassName: "transparent" });
      }
    } else {
      if (this.state.headerWrapClassName) {
        this.setState({ headerWrapClassName: "" });
      }
    }
  };

  render() {
    return (
      <>
        <Navbar
          expand="lg"
          bg="light"
          fixed="top"
          className={`app-header-menu-wrap ${this.state.headerWrapClassName}`}
          collapseOnSelect
        >
          <Container>
            <Navbar.Brand href="#home">
              <img
                alt=""
                src={logo}
                width="30"
                height="30"
                className="d-inline-block align-top"
              />
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="dish-navbar" />
            <Navbar.Collapse id="dish-navbar">
              <Nav className="mr-auto">
                <Nav.Link className="nav-menu-item" href="#OurRestaurant">
                  Our Restaurant
                </Nav.Link>
                <Nav.Link className="nav-menu-item" href="#Menu" active>
                  Menu
                </Nav.Link>
                <Nav.Link className="nav-menu-item" href="#ContactUs">
                  Contact us
                </Nav.Link>
              </Nav>
              <Nav>
                <Form.Group>
                  <InputGroup>
                    <Form.Control
                      type="text"
                      placeholder="Try « Chicken cotoletta »"
                      onChange={(e) => this.props.onChangeSearh(e.target.value)}
                    />
                    <InputGroup.Append>
                      <InputGroup.Text>
                        <Search />
                      </InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                </Form.Group>

                <Nav.Link href="#profile">
                  <span className="app-header-user-login">John C.</span>
                  <Person className="app-header-user-icon" size={24} />
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <div className="app-page-header">
          <div className="container container-xl center">
            <span className="page-title">Menu</span>
          </div>
        </div>
      </>
    );
  }
}
Header.propTypes = {
  onChangeSearh: PropTypes.func.isRequired,
};

Header.defaultProps = {
  onChangeSearh: () => {},
};

export default Header;
