import PropTypes from "prop-types";
import React, { Component } from "react";
import DishListContainer from "../dish/DishListContainer";

export class Content extends Component {
  render() {
    return (
      <div>
        <DishListContainer searchInputValue={this.props.searchInputValue} />
      </div>
    );
  }
}

Content.propTypes = {
  searchInputValue: PropTypes.string,
};

Content.defaultProps = {
  searchInputValue: "",
};

export default Content;
