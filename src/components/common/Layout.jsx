import React, { Component } from "react";
import Content from "./Content";
import Header from "./Header";

export class Layout extends Component {
  constructor() {
    super();
    this.state = {
      searchInputValue: "",
    };
  }
  onChangeSearh = (value) => {
    this.setState({ searchInputValue: value });
  };

  render() {
    return (
      <div className="layout">
        <Header onChangeSearh={this.onChangeSearh} />
        <Content searchInputValue={this.state.searchInputValue} />
      </div>
    );
  }
}

export default Layout;
