### Test (React developer / front-end)

---

## In the project directory, you can run new commands with package manager yarn or npm:

### YARN
1) `yarn install`  

2) `yarn start` 

#### or

### NPM
1) `npm install`

2) `npm start` 

---

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

One problem wasn't fixed: image with large size impossible to set in browser local storage to store data (QuotaExceededError in localStorage.setItem)
